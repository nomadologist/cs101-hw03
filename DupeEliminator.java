/**
 * Eliminates duplicate numbers in an input array.
 * 
 * @author Jack Booth
 * @version 10/4/16
 */
import java.util.Arrays;

public class DupeEliminator
{
	public static void main(String[] args)
	{
		
		int[] original = {1, 1, 2, 7, 7, 7, 4, 5, 8};
		System.out.println("The original array is: " + Arrays.toString(original));
		original = eliminate(original);
		System.out.println("The array without duplicates is " + Arrays.toString(original));
	}
	/**
	 * 
	 * @param original is the array to check for duplicates
	 * @return the new array without any duplicates.
	 */
	private static int[] eliminate(int[] original)
	{
//		integer to keep track of the size of the output array
		int finish = original.length;
		
//		First loop starting at 0 and looping through each element of the array.
		for(int i = 0; i < finish; i++)
		{
//			Second loop starting at one past the current position of the first loop and looping through the array.
			for(int j = (i+1); j < finish; j++)
			{
//				If there is a duplicate
				if(original[i] == original[j])
				{
//					Move the duplicate to the end of the array
					original[j] = original[finish - 1];
					finish--;
					j--;
				}
			}
		}
//		Copy the array, stopping at the determined size of the output array (finished).
		int[] sorted = new int[finish];
		System.arraycopy(original, 0, sorted, 0, finish);
		return sorted;
	}
}
